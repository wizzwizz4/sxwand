from __future__ import annotations

import re
from typing import NamedTuple  # used in non-type code
from typing import Awaitable, Callable, Match, Pattern, TYPE_CHECKING


if TYPE_CHECKING:
	Endpoint = Callable[[
		'Site',
		Match,
		'AsgiCtx',
	], Awaitable]
##	UrlScheme = list[tuple[Pattern, 'UrlScheme'] | [None, Endpoint]]
	UrlScheme = list[tuple[Pattern, Endpoint]]  # KISS, my friend

class AsgiCtx(NamedTuple):
	scope: dict
	receive: Callable[[], Awaitable[dict]]
	send: Callable[[dict], Awaitable]

class Site:
	urls: UrlScheme

	def __init__(self):
		self.urls = []

	async def __call__(
		self,
		scope: dict,
		receive: Callable[[], Awaitable[dict]],
		send: Callable[[dict], Awaitable],
	) -> None:
		assert 'asgi' in scope, "Something wicked this way comes."
		if scope['type'] != 'http':
			raise ValueError("Unknown protocol", scope['type'])

		if scope['scheme'] != 'https':
			pass  # disable login UI

		path = scope['path']
		asgi = AsgiCtx(scope, receive, send)
		for pattern, endpoint in self.urls:
			match = pattern.fullmatch(path)
			if match is None:
				continue
			await endpoint(self, match, asgi)
			return

		await send({
			'type': 'http.response.start',
			'status': 404,
			'headers': [],
			'trailers': False,
		})
		await send({
			'type': 'http.response.body',
			'body': b"Not found.",
			'more_body': False,
		})

	def add(self, pattern: str, flags: int = 0):
		expr = re.compile(pattern, flags)
		def adder(f: Endpoint) -> Endpoint:
			self.urls.append((expr, f))
			return f
		adder.__name__ = f"{pattern!r}#{flags}_adder"  # debuggability
		return adder


fe = Site()

@fe.add(r"/hello_world/")
async def hello_world(self: 'Site', match: Match, asgi: AsgiCtx):
	scope, receive, send = asgi
	await send({
		'type': 'http.response.start',
		'status': 200,
		'headers': [],
		'trailers': False,
	})
	await send({
		'type': 'http.response.body',
		'body': b"Hello, world!",
		'more_body': False,
	})

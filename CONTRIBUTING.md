## Style guide
  * Tabs for indentation, spaces for alignment.
      * [Elastic tabstops] for alignment in the future,
        if they end up being popular with the core developers.
  * "Double quotes for text," and single quotes for 'identifiers'.
  * Single / double blank line / form feed is for meaning, not grammar.
  * Use International Web Standard English spellings for identifiers.
    (i.e., color, labelled, referrer, practising)

Otherwise, default to [PEP-8].

  [PEP-8]: https://peps.python.org/pep-0008/
  [elastic tabstops]: https://nickgravgaard.com/elastic-tabstops/

# SXWand

## Running
You want to run SXWand? Run `sxwand:fe` in the ASGI server.

Hey!

Install the ASGI server (here `uvicorn`: others are avaiable):

    $ sudo apt install uvicorn

or [create][create-venv] and [activate][activate-venv] a venv, then:

    $ pip install 'uvicorn[standard]'

Run the ASGI server:

    $ uvicorn --reload sxwand:fe
    INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
    INFO:     Started reloader process [1299766] using statreload
    INFO:     Started server process [1299768]
    INFO:     Waiting for application startup.
    INFO:     ASGI 'lifespan' protocol appears unsupported.
    INFO:     Application startup complete.

Now visit the URL. The new front-end from HTML City.


  [create-venv]: https://docs.python.org/3/library/venv.html#creating-virtual-environments
  [activate-venv]: https://docs.python.org/3/library/venv.html#how-venvs-work
